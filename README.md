# big-data-demo
# Guincho Power


## Goal: Implement a data solution using:

  - An Infrastructure as Code framework (Terraform) and modern development practices (CI/CD, GitOps)
  - A public cloud providers (AWS)
  - Containerization technologies and container orchestration (Kubernetes, Docker)
  - Different distributed technologies (S3, DynamoDB)
  - Java / Go / Python / Scala / other languages
